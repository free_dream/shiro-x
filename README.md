# shiroX

#### 介绍
shiroX以token作为认证方式，以非常简单的方式就能实现基于token的认证鉴权。



#### 使用说明

1.  将shiroX引入项目
2.  创建TestInfo类 并继承 DefaultAuthenticationAuthorizationInfo
```

public class TestInfo extends DefaultAuthenticationAuthorizationInfo {


    @Override
    public String getAuthenticationInfo(String token) { 
        //获取当前登录账号缓存的token并返回 
        //shiroX会将传入的token和缓存中获取到的token进行对比，
        //如果相同则认证成功否则认证失败
        return token;
    }

    @Override
    public RolePermissionInfo getAuthorizationInfo(String token) {
        //获取到当前登录账号的角色或权限信息并放入RolePermissionInfo中返回
        HashSet<String> permission = new HashSet<>();
        permission.add("test:test");
        RolePermissionInfo rolePermissionInfo = new RolePermissionInfo();
        rolePermissionInfo.setPermissions(permission);
        HashSet<String> roles = new HashSet<>();
        roles.add("admin");
        rolePermissionInfo.setRoles(roles);
        return rolePermissionInfo;
    }
}
```
可以看到DefaultAuthenticationAuthorizationInfo需要实现两个方法
- 1 getAuthenticationInfo， 该方法的参数是从请求头获取到的token值，该方法需要返回
你存储的用户token
- 2 getAuthorizationInfo, 该方法的参数是从请求头获取到的token值, 
该方法需要返回一个RolePermissionInfo对象，这个对象包含了角色信息和权限信息，
用于后续的鉴权操作

2.创建ShiroConfig配置类
```

/**
 * shiro 配置
 * @author xp
 */
@Configuration
public class ShiroConfig {
    /**
     * 自己定义的AuthenticationAuthorizationInfo
     */
    @Bean
    public TestInfo testInfo(){
        return new TestInfo();
    }

    /**
     * 安全管理器 不要使用shiro的安全过滤器 要使用SecurityManagerX
     */
    @Bean
    public SecurityManagerX securityManagerX(){
        SecurityManagerX securityManagerX = new SecurityManagerX();
        //将testInfo放入securityManagerX中
        securityManagerX.setAuthenticationAuthorizationInfo(testInfo());
        return securityManagerX;
    }
    
    /**
     * 过滤器工厂 不要使用shiro的过滤器工厂要使用ShiroFilterFactoryBeanX
     */
    @Bean
    public ShiroFilterFactoryBeanX shiroFilterFactoryBeanX(){
        ShiroFilterFactoryBeanX factoryBeanX = new ShiroFilterFactoryBeanX();
        //设置token在请求头中的key
        factoryBeanX.setAuthorizationName("Authorization");
        //设置未认证转发路径
        factoryBeanX.setUnauthorizedUrl("/shiroX/unauthorized");
        //设置安全管理器
        factoryBeanX.setSecurityManager(securityManagerX());
        //设置过滤条件
        HashMap<String, String> filterChainMap = new HashMap<>();
        //不拦截路径
        filterChainMap.put("/shiroX/login", "anno");
        //拦截路径 (这里要使用filerX)
        filterChainMap.put("**", "filterX");
        factoryBeanX.setFilterChainDefinitionMap(filterChainMap);
        return factoryBeanX;
    }
}
```
以上就是shiroX的所有配置

3.token生成

- 根据设置的key生成token

```
        String token = ShiroUtil.getTokenFactory().
                builder(TestInfo.class).
                buildKey("username", "xp").
                buildKey("id", "1").
                buildSecret("123456").
                generateTokenFromKeys();
```
- 根据对象属性生成token
```
        User user = new User();
        user.setUsername("shiroX");
        user.setPhone("15199933333");

        String token = ShiroUtil.getTokenFactory().
                builder(TestInfo.class).
                buildObject(user).
                buildSecret("123456").
                generateTokenFromObject();
```
- 通过ShiroUtil.getTokenFactory().builder(TestInfo.class) 可以获取token构建器
TestInfo.class就是上面创建的TestInfo对象

- 多种账号token
    多种账号生成token只需要编写不同的DefaultAuthenticationAuthorizationInfo并且
    使用token生成器生成token即可，shiroX会自动匹配不同种类账号的token
- token解析 
```
        String username = ShiroUtil.getTokenFactory().decoder(token)
                         .decodeKey("username");
```
- 将token解析成对象

```
        User user = ShiroUtil.getTokenFactory().
        decoder(token).
        getObject(User.class);
```
- 将token解析为map
```
        Map<String, String> all = JwtUtil.getAll(token);
```
- 获取当前登录账号token

``` 
        String token = ShiroUtil.getToken();
```

- 将当前登录账号token解析成对象

```
        User user = ShiroUtil.getTokenObject(User.class);
```