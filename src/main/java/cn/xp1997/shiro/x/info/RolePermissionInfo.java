package cn.xp1997.shiro.x.info;

import java.util.Set;

/**
 * @author xp
 */
public class RolePermissionInfo {

    private Set<String> roles;

    private Set<String> permissions;

    public void setRoles(Set<String> roles){
        this.roles = roles;
    }

    public Set<String> getRoles(){
        return this.roles;
    }

    public void setPermissions(Set<String> permissions){
        this.permissions = permissions;
    }

    public Set<String> getPermissions(){
        return this.permissions;
    }

}
