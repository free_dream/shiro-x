package cn.xp1997.shiro.x.info;

import cn.xp1997.shiro.x.info.authentication.AuthenticationInfo;
import cn.xp1997.shiro.x.info.authorization.AuthorizationInfo;

/**
 *
 * @author xp
 */
public interface AuthenticationAuthorizationInfo extends AuthenticationInfo, AuthorizationInfo {



    boolean tokenSupport(String token);

}
