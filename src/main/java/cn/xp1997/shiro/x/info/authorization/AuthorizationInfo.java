package cn.xp1997.shiro.x.info.authorization;


import cn.xp1997.shiro.x.info.RolePermissionInfo;

/**
 * 授权信息
 * @author xp
 */
public interface AuthorizationInfo {

    /**
     * 获取授权信息
     * @param token token
     * @return 授权信息
     */
    RolePermissionInfo getAuthorizationInfo(String token);

}
