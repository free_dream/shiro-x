package cn.xp1997.shiro.x.info;


import cn.xp1997.shiro.x.constant.TokenConstant;
import cn.xp1997.shiro.x.utils.ShiroUtil;

/**
 *
 * @author xp
 */
public abstract class DefaultAuthenticationAuthorizationInfo implements AuthenticationAuthorizationInfo {


    @Override
    public boolean tokenSupport(String token){
        return this.getClass().getName().equals(
                ShiroUtil.
                        getTokenFactory().
                        decoder(token).
                        decodeKey(TokenConstant.TOKEN_SIGN_KEY_INFO)
        );
    }

}
