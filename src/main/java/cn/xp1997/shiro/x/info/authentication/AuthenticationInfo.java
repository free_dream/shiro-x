package cn.xp1997.shiro.x.info.authentication;

/**
 *
 * @author xp
 */
public interface AuthenticationInfo {

    /**
     *
     * @param token token
     * @return token
     */
    String getAuthenticationInfo(String token);

}
