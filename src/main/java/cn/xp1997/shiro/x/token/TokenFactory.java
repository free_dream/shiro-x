package cn.xp1997.shiro.x.token;


/**
 * token生成工厂
 * @author xp
 */
public class TokenFactory {

    public TokenBuilder builder(Class infoClass){
        return new TokenBuilder().buildInfo(infoClass);
    }


    public TokenDecoder decoder(String token){
        return new TokenDecoder(token);
    }

}
