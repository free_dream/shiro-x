package cn.xp1997.shiro.x.token;

import org.apache.shiro.authc.AuthenticationToken;


/**
 * Token
 * @author xp
 */
public class TokenX implements AuthenticationToken  {

    private String token;

    public TokenX(String token){
        this.token = token;
    }

    @Override
    public Object getPrincipal() {
        return token;
    }


    @Override
    public Object getCredentials() {
        return token;
    }
}
