package cn.xp1997.shiro.x.subject;

public class SubjectFactoryX {

    private ThreadLocal<SubjectX> subjectThreadLocal = new ThreadLocal<>();

    public void createSubjectX(String token){
        SubjectX subjectX = new SubjectX();
        subjectX.setToken(token);
        subjectThreadLocal.set(subjectX);
    }

    public SubjectX getSubject(){
        SubjectX subjectX = subjectThreadLocal.get();
        subjectThreadLocal.remove();
        return subjectX;
    }

}
