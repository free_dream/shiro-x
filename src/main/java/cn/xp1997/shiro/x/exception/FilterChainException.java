package cn.xp1997.shiro.x.exception;

/**
 * 过滤器链异常
 * @author xp
 */
public class FilterChainException extends RuntimeException{

    public FilterChainException(){
        super();
    }

    public FilterChainException(String message){
        super(message);
    }

}
