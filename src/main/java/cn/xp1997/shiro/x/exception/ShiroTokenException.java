package cn.xp1997.shiro.x.exception;

/**
 * token异常
 * @author xp
 */
public class ShiroTokenException extends RuntimeException{

    public ShiroTokenException(){
        super();
    }

    public ShiroTokenException(String message){
        super(message);
    }

}
