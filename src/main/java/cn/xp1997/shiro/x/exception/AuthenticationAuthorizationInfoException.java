package cn.xp1997.shiro.x.exception;

/**
 * info 异常
 * @author xp
 */
public class AuthenticationAuthorizationInfoException extends RuntimeException{

    public AuthenticationAuthorizationInfoException(){
        super();
    }

    public AuthenticationAuthorizationInfoException(String message){
        super(message);
    }

}
