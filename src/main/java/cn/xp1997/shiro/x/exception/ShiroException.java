package cn.xp1997.shiro.x.exception;


public class ShiroException extends RuntimeException{

    public ShiroException(String message){
        super(message);
    }

}
