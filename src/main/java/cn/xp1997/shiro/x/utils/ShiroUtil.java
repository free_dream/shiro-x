package cn.xp1997.shiro.x.utils;

import cn.xp1997.shiro.x.security.SecurityManagerX;
import cn.xp1997.shiro.x.subject.SubjectX;
import cn.xp1997.shiro.x.token.TokenFactory;

/**
 * @author xp
 *
 */
public class ShiroUtil {

    public static SecurityManagerX securityManagerX;

    public static TokenFactory tokenFactory;

    public static SubjectX getSubjectX(){
        return getSecurityManagerX().getSubjectFactoryX().getSubject();
    }

    public static SecurityManagerX getSecurityManagerX(){
        return securityManagerX;
    }

    public static TokenFactory getTokenFactory(){
        return tokenFactory;
    }

    /**
     * 获取token对象
     */
    public static <T> T getTokenObject(Class<T> tClass){
        return getTokenFactory().decoder(getSubjectX().getToken()).getObject(tClass);
    }

    public static String getToken(){
        return getSubjectX().getToken();
    }



}
