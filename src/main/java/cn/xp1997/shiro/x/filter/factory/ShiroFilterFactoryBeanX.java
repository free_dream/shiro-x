package cn.xp1997.shiro.x.filter.factory;

import cn.xp1997.shiro.x.filter.FilterX;
import cn.xp1997.shiro.x.security.SecurityManagerX;
import org.apache.shiro.spring.web.ShiroFilterFactoryBean;

import javax.servlet.Filter;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * filterFactoryBean
 * @author xp
 */

public class ShiroFilterFactoryBeanX extends ShiroFilterFactoryBean{

    private SecurityManagerX securityManager;

    private FilterX filterX;

    private String authorizationName;

    public FilterX getFilterX(){
        return this.filterX;
    }

    public void setAuthorizationName(String name){
        getFilterX().setAuthorizationName(name);
        this.authorizationName = name;
    }

    public String getAuthorizationName(){
        return this.authorizationName;
    }

    public ShiroFilterFactoryBeanX(){
        //初始化
        initShiroFilterFactoryBeanX();

    }

    /**
     * 初始化
     */
    private void initShiroFilterFactoryBeanX(){
        //设置FilterX
        setFilterX();
    }

    /**
     * 设置filterX
     */
    private void setFilterX(){
        // 添加 自定义过滤器
        Map<String, Filter> filterMap = new LinkedHashMap<>();
        filterMap.put("filterX", createFilterX());
        this.setFilters(filterMap);
    }

    @Override
    public void setUnauthorizedUrl(String unauthorizedUrl) {
        super.setUnauthorizedUrl(unauthorizedUrl);
        getFilterX().setUnauthorizedUrl(unauthorizedUrl);
    }

    /**
     * 创建FilterX
     */
    private FilterX createFilterX(){
        FilterX filterX = new FilterX();
        return this.filterX = filterX;
    }


    public void setSecurityManager(SecurityManagerX securityManager) {
        this.securityManager = securityManager;
    }

    @Override
    public SecurityManagerX getSecurityManager(){
        return this.securityManager;
    }

}
