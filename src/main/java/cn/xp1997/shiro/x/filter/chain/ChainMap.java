package cn.xp1997.shiro.x.filter.chain;

import java.util.LinkedHashMap;


/**
 * 过滤器条件map
 * @author xp
 */
public interface ChainMap {


    /**
     * 获取过滤链条件map
     * @return map
     */
    LinkedHashMap<String, String> getFilterChainDefinitionMap();

}
