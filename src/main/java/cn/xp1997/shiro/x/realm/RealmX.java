package cn.xp1997.shiro.x.realm;


import cn.xp1997.shiro.x.token.TokenX;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.subject.PrincipalCollection;


/**
 * realm
 * @author xp
 */
public class RealmX extends InfoRealm {

    @Override
    public boolean supports(AuthenticationToken token){
        return token instanceof TokenX;
    }


    /**
     * 授权
     */
    @Override
    public AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
        return getAuthorizationInfoCache(principalCollection);
    }

    /**
     * 认证
     */
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {
        return getAuthenticationInfoCache(authenticationToken);
    }


}
